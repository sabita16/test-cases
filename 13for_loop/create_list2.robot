# Creating a list cariable @{item} which contains certain value
# to read each and individual value from the list for loop can be used
# FOR  ${i}  IN  @{item} whic is a index
# it gets the 1st value from the list and stores in ${i} or index variable and so on till the end
# log to console  ${i} prints the index value

*** Test Cases ***
creating list with for loop
    @{item}  create list  1 2   3   4   5
    FOR  ${i}  IN  @{item}
        log to console  ${i}
    END