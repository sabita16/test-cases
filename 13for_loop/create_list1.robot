# First creating an empty variable ${i} with assigning the range in it
# after that printing it with "log to console"
# at last ending the loop with END

*** Test Cases ***
For Loop Test
    FOR   ${i}     IN RANGE    1  10
        log to console   ${i}
    END