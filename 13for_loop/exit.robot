# Exits from the loop upon the given favourable condition

*** Test Cases ***
with Exit
    @{item}  create list  1  2  3  4  5
    FOR  ${i}  IN  @{item}
        log to console  ${i}
        exit for loop if  ${i}==3
    END