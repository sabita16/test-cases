#

*** Settings ***
Library    SeleniumLibrary

*** Test Cases ***
Mouse Actions
    [Documentation]         This test case performs different mouse actions e.g right click - opens the context menu
    ...                     double clicking, drag and drop
    set selenium speed      0.4
    open browser            https://testautomationpractice.blogspot.com/    chrome
    open context menu       id:datepicker
    click element           xpath://*[@id="ui-datepicker-div"]/table/tbody/tr[5]/td[3]/a
    # Double click action
    double click element    xpath://*[@id="HTML10"]/div[1]/button
    # drag and drop
    drag and drop           xpath://*[@id="gallery"]/li[1]/img              id:trash
    close browser




