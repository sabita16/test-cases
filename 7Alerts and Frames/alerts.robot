
*** Settings ***
Library  SeleniumLibrary

*** Test Cases ***
Handling alerts
    [Documentation]         This test case works on handling different alerts.
    set selenium speed      0.4
    open browser            https://demoqa.com/alerts   chrome
    click button            xpath://*[@id="alertButton"]
    handle alert            accept  # accepting the alert
    #handle alert    dismiss #for canceling and closing the alert  button
    #handle alert    leave  # keeps on open the alert without closing until and unless we close
    # alert should be present     You clicked a button # for verifying special alert/text
    close all browser

