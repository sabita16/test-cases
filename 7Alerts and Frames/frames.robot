
*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
Testing Frames
    [Documentation]     This test case works on handling frames.
    ...                 While unselecting, there is no need to specify any frame as it will unselect the frame selected above
    open browser        https://ui.vision/demo/webtest/frames/      chrome
    select frame        name2
    click element       xpath://*[@id="id2"]/div/input
    unselect frame
    select frame        name3 # selecting another frame in the same browser
    click element       xpath://*[@id="mG61Hd"]/div[2]/div[1]/div[2]/div[1]/div/div/div[2]/div[1]/div/span/div/div[2]/label/div/div[2]/div
    unselect frame
    select frame        name4
    click element       xpath://*[@id="id4"]/div/input
    unselect frame
    close browser

