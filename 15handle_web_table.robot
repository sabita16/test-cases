
*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${url}          https://sqengineer.com/practice-sites/practice-tables-selenium/
${browser}      chrome

*** Test Cases ***
Table operation
    [Documentation]         This test case is about handling web table. First variables are created for counting
    ...                     element in rows and tables and then printing them.
    ...                     Example: To capture the data from the table example 5th row and 1st column
    ...                     get text  xpath://*[@id="table1"]/tbody/tr[5]/td[1]
    ...                     and then storing it in a variable.
    ...                     validation is done in the end.
    set selenium speed      0.4
    open browser            ${url}   ${browser}
    ${rows}                 get element count  xpath://*[@id="table1"]/tbody/tr
    ${columns}              get element count  xpath://*[@id="table1"]/tbody/tr[1]/th
    log to console          ${rows}
    log to console          ${columns}
    ${data}                 get text  xpath://*[@id="table1"]/tbody/tr[5]/td[1]
    log to console          ${data}
    table column should contain  xpath://*[@id="table1"]  2     Type
    table row should contain     xpath://*[@id="table1"]  4     Ranorex
    table cell should contain    xpath://*[@id="table1"]  2  2  Open Source
    table header should contain  xpath://*[@id="table1"]        Automation Tool
    # table footer should contain
    close browser