*** Settings ***
Library  SeleniumLibrary

*** Test Cases ***
closing browsers
    [Documentation]     working with closing browser/s
    open browser        https://www.techlistic.com/p/selenium-practice-form.html    chrome
    open browser        http://automationpractice.com/index.php                     chrome
    open browser        https://phptravels.com/demo/                                chrome
    open browser        https://www.techlistic.com/p/demo-selenium-practice.html    chrome

    close browser
    close all browsers

