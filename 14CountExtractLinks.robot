
*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${browser}      chrome
${url}          https://demoqa.com/

*** Test Cases ***
Get All Links
    [Documentation]     This test case works on:
    ...                 Counting number of links on the given web page
    ...                 Extracting all the links from the given web page
    ...                 'get element count' keyword gives the number of elements present in the page based on the locator passed
    ...                 Extracting each and every link from the web page; First creating a list
    open browser        ${url}    ${browser}
    ${alllinkscount}    get element count   xpath://a
    log to console      ${alllinkscount}
    @{linklist}         create list
    FOR  ${i}  IN RANGE   1  ${alllinkscount}
        ${linktext}     get text  xpath:(//a)[${i}]
        log to console      ${linktext}
    END
    close browser

