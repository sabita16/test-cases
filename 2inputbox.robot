
*** Settings ***
Library             SeleniumLibrary

*** Variables ***
${browser}          chrome
${url}              https://demo.nopcommerce.com/

*** Test Cases ***
TestingInputBox
    [Documentation]             This test case works with different web elements. For an example:
    ...                         Text box/Input Box, verification of title of the page, visibility status,
    ...                         enabled status, providing value, clearing value are done
    open browser                ${url}   ${browser}
    maximize browser window
    title should be             nopCommerce demo store
    click link                  xpath:/html/body/div[6]/div[1]/div[1]/div[2]/div[1]/ul/li[2]/a
    ${"email_txt"}              set variable      id:Email

    element should be visible   ${"email_txt"}
    element should be enabled   ${"email_txt"}

    input text                  ${"email_txt"}     sabitamanandhar16@gmail.com
    sleep                       5
    clear element text          ${"email_txt"}
    sleep                       3
    close browser








