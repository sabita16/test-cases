
*** Settings ***
Library    SeleniumLibrary

*** Test Cases ***
Navigation Test
    [Documentation]     This test case navigates bewteen multiple url/pages with browser related keywords.
    ...                 go to : goes to new url
    ...                 go back: go back to previous page/url
    ...                 get location: gives the current url of application
    open browser        https://www.google.com/     chrome
    ${location}         get location
    log to console      ${location}
    go to               https://www.youtube.com/ # a new page opens in the same browser
    ${location}         get location
    log to console      ${location}
    go back
    close all browsers


