# Working on a table:
# Counting number of rows
# Counting number of columns
# Get/extract data
# setting Validations


*** Settings ***
Library  DatabaseLibrary
Library  OperstingSystem

Suite Setup  Connect to Database   pymysql  ${name}  ${user}  ${password}  ${host}  ${port}
Suite Teardown  Disconnect from Database

*** Variables ***
${name}     mydb
${user}     root
${password} root
${host}     127.0.0.1
${port}     3306