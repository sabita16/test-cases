
*** Settings ***
Library      SeleniumLibrary

*** Variables ***
${browser}      chrome
${url}          https://www.techlistic.com/p/selenium-practice-form.html

*** Test Cases ***
Drop downs and List boxes
    [Documentation]                 working with Drop downs and List boxes
    open browser                    ${url}   ${browser}
    maximize browser window
    click button                    xpath://*[@id="ez-accept-all"]
    set selenium speed              2 seconds
    select from list by label       continents          Europe
    select from list by label       selenium_commands   Navigation Commands
    select from list by label       selenium_commands   Wait Commands
    unselect from list by label     selenium_commands   Navigation Commands
    close browser