

*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${url}          https://demo.nopcommerce.com/
${browser}      chrome

*** Test Cases ***
Creating user defined keyword
    [Documentation]     This test case uses User Defined Keywords: without arguments
    launchbrowser
    click element       xpath:/html/body/div[6]/div[1]/div[1]/div[2]/div[1]/ul/li[1]/a
    input text          name:FirstName    Sabita
    input text          name:LastName     Manandhar
    close browser

*** Keywords ***
launchbrowser
    [Documentation]     User defined keywords are created under this (Keywords) section
    ...                 while calling the user created keyword in test cases, those steps should be executed in test cases
    open browser        ${url}   ${browser}
    maximize browser window
