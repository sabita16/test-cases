
*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${url}      https://demo.nopcommerce.com/
${browser}  chrome

*** Test Cases ***
Creating user defined keyword
    [Documents]     This test case uses User Defined Keywords: with arguments.
    ...             calling user defined with two arguments.
    ...             in this case, url will capture app url and browser will captures appbrowser
    ...             and execute the rest of the steps as given
    launchbrowser   ${url}              ${browser}
    click element   xpath:/html/body/div[6]/div[1]/div[1]/div[2]/div[1]/ul/li[1]/a
    input text      name:FirstName      Sabita
    input text      name:LastName       Manandhar
    close browser

*** Keywords ***
launchbrowser
    [Documents]     while calling the user created keyword in test cases, those steps should be executed in test cases
    [Arguments]     ${appurl}    ${appbrowser}   # only two arguments are accepted
    open browser    ${appurl}    ${appbrowser}
   maximize browser window