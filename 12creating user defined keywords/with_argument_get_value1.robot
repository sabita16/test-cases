
*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${url}      https://demo.nopcommerce.com/
${browser}  chrome

*** Test Cases ***
Creating user defined keyword
    [Documents]     This test case uses User Defined Keywords: with arguments and return value
    ...             calling user defined with two arguments.
    ...             in this case, url will capture app url and browser will captures appbrowser
    ...             and execute the rest of the steps as given
    set selenium speed  0,5
    ${pagetitle}        launchbrowser       ${url}      ${browser}
    log to console      ${pagetitle}
    click element       xpath:/html/body/div[6]/div[1]/div[1]/div[2]/div[1]/ul/li[1]/a
    input text          name:FirstName      Sabita
    input text          name:LastName       Manandhar
    close browser

*** Keywords ***
launchbrowser
     [Documents]    User defined keywords are created under this (Keywords) section
    ...             while calling the user created keyword in test cases, those steps should be executed in test cases
    ...             only two arguments are accepted
    ...             after executing all these steps, if needs to return the value, example getting the title of the page
    ...             the returned value need to be in a variable which is created in the test cases
    ...             as a  # ${pagetitle} and then print the value
    [Arguments]     ${appurl}    ${appbrowser}
    open browser    ${appurl}    ${appbrowser}
    maximize browser window
    ${title}        get title     # creating a variable and storing the title in it
    [Return]        ${title}      # return back the assigned value
