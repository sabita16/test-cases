

*** Settings ***
Library     SeleniumLibrary
Resource    ../Resources/resource1.robot

*** Variables ***
${url}      https://demo.nopcommerce.com/
${browser}  chrome

*** Test Cases ***
Creating user defined keyword
    [Documentation]     This test case uses User Defined Keywords: with arguments and return value
    ...                 using the external file/resource1.robot
    ...                 calling user defined with two arguments.
    ...                 url will capture app url and browser will captures appbrowser
    ...                 and execute the rest of the steps as given
    ${pagetitle}        launchbrowser       ${url}      ${browser}
    log to console      ${pagetitle}
    click element       xpath:/html/body/div[6]/div[1]/div[1]/div[2]/div[1]/ul/li[1]/a
    input text          name:FirstName      Sabita
    input text          name:LastName       Manandhar
    close browser

