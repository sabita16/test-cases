
*** Settings ***
Documentation       logging in the browser using variables
Library             SeleniumLibrary

*** Variables ***
${browser}      chrome
${url}          https://demo.nopcommerce.com/

*** Test Cases ***
Logintest
    open browser     ${url}        chrome
    click link      xpath:/html/body/div[6]/div[1]/div[1]/div[2]/div[1]/ul/li[2]/a
    input text      id:Email     sabitamanandhar16@gmail.com
    input text      id:Password  password
    click element   xpath:/html/body/div[6]/div[3]/div/div/div/div[2]/div[1]/div[2]/form/div[3]/button
    close browser
