
*** Settings ***
Library    SeleniumLibrary


*** Test Cases ***
Tabbed Windows Test
    [Documentation]     Some browser can redirect to another differet browser/page/application
    ...                 if needed to do any operation in the second browser, switch need to be done from first to second page
    open browser        http://demo.automationtesting.in/Windows.html   chrome
    set selenium speed  3 seconds
    click element       xpath://*[@id="Tabbed"]/a/button
    # it will redirect to different new window
    switch window       url:https://www.selenium.dev/
    click element       xpath://*[@id="main_navbar"]/ul/li[5]/a/span
    close all browsers




