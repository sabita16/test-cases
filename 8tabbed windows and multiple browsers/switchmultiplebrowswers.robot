
*** Settings ***
Library    SeleniumLibrary


*** Test Cases ***
Multiple Browser Switch Test
    [Documentation]     This test case switches multiple browser.
    ...                 switch between these multiple browsers can be perform
    ...                 whenenever working with multiple browsers, the index numbering of them is started.
    ...                 it starts from 1,2,3 and so on depending on the number of browsers
    open browser        https://www.google.com/     chrome
    maximize browser window
    open browser        https://www.youtube.com/    chrome
    maximize browser window
    switch browser      1
    ${title1}           get title
    log to console      ${title1}
    switch browser      2
    ${title2}           get title
    log to console      ${title2}
    close all browsers