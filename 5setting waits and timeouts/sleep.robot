
*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${browser}                  chrome
${url}                      https://demo.nopcommerce.com/

*** Test Cases ***
Logintest
    [Documentation]         This test case execute sleep keyword which slows down the execution within the given time.
    ...                     It is not applicable for all the executions but only on the selected statement
    open browser            ${url}          chrome
    sleep                   2
    click link              xpath:/html/body/div[6]/div[1]/div[1]/div[2]/div[1]/ul/li[2]/a
    sleep                   2
    input text              id:Email        sabitamanandhar16@gmail.com
    sleep                   2
    input text              id:Password     manandhar
    sleep                   2
    click element           xpath:/html/body/div[6]/div[3]/div/div/div/div[2]/div[1]/div[2]/form/div[3]/button
    close browser
