
*** Settings ***
Library      SeleniumLibrary

*** Variables ***
${browser}      chrome
${url}          http://automationpractice.com/

*** Test Cases ***
timeout Test
    [Documentation]         This test case use timeout i.e. if the contains doesnot appears in the browser then how much
    ...                     time can be waited? what is the maximum timeout? the default timeout is 5seconds
    ...                     but timeout can be maximise/set through: set selenium timeout
    ...                     timeout is applicapable only for specific statement and not affect all statement in test case
    ${timeout}              get selenium timeout
    log to console          ${timeout}
    open browser            ${url}          ${browser}
    maximize browser window
    wait until page contains    CREATE AN ACCOUNT
    set selenium timeout    10 seconds
    set selenium speed      2
    click link              //*[@id="header"]/div[2]/div/div/nav/div[1]/a
    input text              name:email_create           sabitamanandhar16@gmail.com
    click element           xpath://*[@id="SubmitCreate"]/span
    click element           xpath:xpath://*[@id="id_gender2"]
    input text              name:customer_firstname     Sabita
    input text              name:customer_lastname      Manandhar
    input password          name:passwd                 beautifullife


