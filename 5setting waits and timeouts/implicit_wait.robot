
*** Settings ***
Library      SeleniumLibrary

*** Variables ***
${browser}      chrome
${url}          http://automationpractice.com/

*** Test Cases ***
implicit wait
    [Documentation]                 working on implicit wait which will applicapable for all the elements.
    ...                             Suppose if there is an error that certain element is not found and by default
    ...                             0 seconds is waited to look/locate for the element. selenium implicit time
    ...                             can be set so that given time is taken to locate the element.
    open browser                    ${url}      ${browser}
    maximize browser window
    ${implicittime}                 get selenium implicit wait
    log on to console               ${implicittime}
    set selenium implicit wait      0.5 seconds
    click link                      xpath://*[@id="header"]/div[2]/div/div/nav/div[1]/a
    input text                      name:email_create               sabitamanandhar16@gmail.com
    click element                   xpath://*[@id="SubmitCreate"]/span
    click element                   xpath://*[@id="id_gender2"]
    input text                      name:customer_firstname         Sabita
    input text                      name:customer_lastname          Manandhar
    input password                  name:passwd                     beautifullife
