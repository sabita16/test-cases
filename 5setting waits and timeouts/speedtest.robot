*** Settings ***
Library      SeleniumLibrary

*** Variables ***
${browser}      chrome
${url}          http://automationpractice.com/

*** Test Cases ***
Registration Test
    [Documentation]         This test case executes the
    ${speed}                get selenium speed
    log to console          ${speed}
    open browser            ${url}                  ${browser}
    maximize browser window
    set selenium speed      2
    click link              //*[@id="header"]/div[2]/div/div/nav/div[1]/a
    input text              name:email_create               sabitamanandhar16@gmail.com
    click element           xpath://*[@id="SubmitCreate"]/span

    click element           xpath://*[@id="id_gender2"]
    input text              name:customer_firstname         Sabita
    input text              name:customer_lastname          Manandhar
    input password          name:passwd                     beautifullife
    ${speed}                get selenium speed
    log to console          ${speed}