*** Settings ***
Library  SeleniumLibrary
Resource  ../PageObjectsModel/loginKeywords.robot

*** Variables ***
${browser}   chrome
${siteurl}   https://demo.nopcommerce.com/login?returnUrl=%2F
${username}  sabitamanandhar16@gmail.com
${password}  password

*** Test Cases ***
Login Test
    open my browser    ${siteurl}    ${browser}
    sleep  3
    enter username    ${username}
    sleep  3
    enter password    ${password}
    sleep  3
    click signin
    sleep  3
    verify successfullogin
    closing my browser