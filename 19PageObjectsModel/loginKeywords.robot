*** Settings ***
Library     SeleniumLibrary
Variables   ../PageObjectsModel/locators.py

*** Keywords ***
open my browser
    [Arguments]    ${siteurl}  ${browser}
    open browser   ${siteurl}  ${browser}
    maximize browser window

enter username
    [Arguments]   ${username}
    input text    ${loginUsername}   ${username}

enter password
    [Arguments]   ${password}
    input text   ${loginPassword}   ${password}

click signin
    click button  ${signingIn}

verify successfullogin
    title should be   nopCommerce demo store

closing my browser
    close all browsers



