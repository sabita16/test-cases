
*** Settings ***
Documentation       This test contains the resource file. Suite suiteup, Suite Teardown and Test Template is set.
Library             SeleniumLibrary
Resource            ../Resources/login_resource.robot
Suite Setup         open my browser
Suite Teardown      close browsers
Test Template       Invalid Login

*** Test Cases ***
right user & empty password    sabitamanandhar16@gmail.com   ${EMPTY}
right user & wrong password    sabitamanandhar16@gmail.com   abc
wrong user & right password    sabitamanandhar@gmail.com   password
wrong user & empty password    sabitamanandhar@gmail.com   ${EMPTY}
wrong user & wrong password    sabitamanandhar@gmail.com   abc



*** Keywords ***
Invalid Login
    [Arguments]   ${username}  ${password}
    input username   ${username}
    input password   ${password}
    click login button
    error message should be visible

