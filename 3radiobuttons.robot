
*** Settings ***
Library      SeleniumLibrary

*** Variables ***
${browser}      chrome
${url}          https://www.techlistic.com/p/selenium-practice-form.html

*** Test Cases ***
Testing Radio Buttons and Check Boxes
    [Documentation]         This test case is working with Radio buttons and Check boxes
    open browser            ${url}          ${browser}
    maximize browser window
    click button            xpath://*[@id="ez-accept-all"]
    set selenium speed      2 seconds
    select radio button     sex     Female
    select radio button     exp     5
    select check box        Automation Tester
    select check box        Selenium Webdriver
    close browser


